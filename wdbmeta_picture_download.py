import os
import requests
from bs4 import BeautifulSoup
from urllib.parse import urljoin

# Basis-URLs für die XML-Dateien und die Grafiken
base_url = "https://exist.ulb.tu-darmstadt.de/2/v/"
base_g_url = "https://exist.ulb.tu-darmstadt.de/2/g/"

# Datei für gescheiterte URLs
failed_urls_file = "failed_urls.txt"

# Funktion zum Speichern von URLs in einer Textdatei
def save_failed_url(url):
    with open(failed_urls_file, 'a') as f:
        f.write(url + '\n')
    print(f"Failed URL saved: {url}")

# Von pa000321 bis pa000332 durchlaufen
for i in range(321, 333):
    url = f"{base_url}pa000{i}-0000-0000"
    
    # Prüfen, ob die URL "exist" enthält
    if "exist" in url:
        print(f"Handling 'exist' URL: {url}")
        
        try:
            response = requests.get(url)
            if response.status_code == 200:
                soup = BeautifulSoup(response.content, 'xml')
                
                # Alle <file>-Elemente durchsuchen
                for file_tag in soup.find_all('file'):
                    xml_id = file_tag['xml:id']
                    file_url = f"{base_g_url}{xml_id}"
                    
                    try:
                        file_response = requests.get(file_url)
                        if file_response.status_code == 200:
                            file_soup = BeautifulSoup(file_response.content, 'xml')
                            
                            # Prüfen, ob <facsimile>-Element vorhanden ist
                            for facsimile in file_soup.find_all('facsimile'):
                                folder_name = xml_id
                                if not os.path.exists(folder_name):
                                    os.makedirs(folder_name)
                                
                                count = 1
                                # Alle <graphic>-Elemente durchsuchen
                                for graphic in facsimile.find_all('graphic'):
                                    img_url = graphic['url']
                                    
                                    # Überprüfen, ob img_url gültig ist und sicherstellen, dass sie korrekt formatiert ist
                                    if not img_url.startswith('http'):
                                        img_url = urljoin(base_g_url, img_url)  # relative URL ergänzen
                                    
                                    try:
                                        # Grafik herunterladen und speichern
                                        img_response = requests.get(img_url)
                                        if img_response.status_code == 200:
                                            img_filename = f"{str(count).zfill(3)}.jpg"
                                            img_path = os.path.join(folder_name, img_filename)
                                            with open(img_path, 'wb') as img_file:
                                                img_file.write(img_response.content)
                                            print(f"Downloaded {img_filename} into {folder_name}")
                                            count += 1
                                        else:
                                            print(f"Failed to download image from {img_url}")
                                            save_failed_url(img_url)
                                    except requests.ConnectionError:
                                        print(f"Connection error for image URL: {img_url}")
                                        save_failed_url(img_url)
                        else:
                            print(f"Failed to download file from {file_url}")
                            save_failed_url(file_url)
                    except requests.ConnectionError:
                        print(f"Connection error for file URL: {file_url}")
                        save_failed_url(file_url)
            else:
                print(f"Failed to access {url}")
                save_failed_url(url)
        except requests.ConnectionError:
            print(f"Connection error for main URL: {url}")
            save_failed_url(url)
    else:
        print(f"Skipping non-'exist' URL: {url}")
