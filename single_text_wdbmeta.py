import os
import requests
from bs4 import BeautifulSoup

url = "https://exist.ulb.tu-darmstadt.de/2/g/pa000008-0128"

folder_name = "pa000008-0128"
os.makedirs(folder_name, exist_ok=True)

response = requests.get(url)
soup = BeautifulSoup(response.content, 'html.parser')

count = 1

for facsimile in soup.find_all('facsimile'):
    for graphic in facsimile.find_all('graphic'):
        img_url = graphic['url']
        
        img_response = requests.get(img_url)
        if img_response.status_code == 200:
            img_filename = f"{str(count).zfill(3)}.jpg"  
            with open(os.path.join(folder_name, img_filename), 'wb') as img_file:
                img_file.write(img_response.content)
            print(f"Downloaded {img_filename}")
            count += 1
        else:
            print(f"Failed to download image from {img_url}")